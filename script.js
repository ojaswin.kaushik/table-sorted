// Write a Program that should contains
// Class Person with fields (name, age, salary, sex)
// a static sort function in the class that should take an array of Persons and name of the field and order of sorting and should return a new sorted array based on above inputs. Should not change initial array.
// for example Person.sort(arr, 'name', 'asc') -> sort array of persons based on name in ascending order. 'desc' for descending
// You have to write a quick sort for this sorting algorithm.

const btn = document.querySelector('.btn')
const table = document.getElementsByTagName('table')[0]
const input = document.getElementsByTagName('input')
const btn2 = document.querySelector('.btn-sort')
const ColName=document.getElementById('Col_Name')
const sortIn=document.getElementById('SortIn')
btn.addEventListener('click',Addrow)
btn2.addEventListener('click',Arraysort)

function NotValid()
{
    //Name Checking 
    if (!(/([a-z]|[A-Z]){6,}/.test(input[0].value)))
    {
        
        return 1
    }
    // age validation
    if ((Number(input[1].value))>100 || (Number(input[1].value))<18)
    {
        
        return 1
    }

    if ((Number(input[2].value))<10000 || (Number(input[2].value))>200000)
    {
        
        return 1
    }
    if (!(["MALE","FEMALE","OTHER"].includes(input[3].value.toUpperCase())))
    {
        
        return 1
    }
}
function Addrow() {
    if (NotValid())
    {
        alert("please enter valid info")
        return
    }
    const new_row = document.createElement('tr')
    for (var i=0;i<4;i++)
    {
        const new_data = document.createElement('td');
        new_data.innerText=input[i].value
        new_row.appendChild(new_data)
    } 
    table.appendChild(new_row)
  }

function Arraysort()
{
    let rows  = Array.from(document.getElementsByTagName('tr'))
    const attr=rows[0]
    rows=rows.slice(1)
    console.log(attr)
    let colIndex=1;
    let fl=1;
    Array.from(attr.getElementsByTagName('th')).forEach(element => {

        if (element.textContent.trim()===ColName.value)
            fl=0
        colIndex+=fl
        
    });
    const SortIn = sortIn.value
    
    console.log(colIndex,SortIn)

    rows = rows.sort((a,b)=>
    {
        const aCol= a.querySelectorAll('td')[colIndex].textContent.trim()
        const bCol = b.querySelectorAll('td')[colIndex].textContent.trim()


        if (SortIn==='asc')
            return aCol>bCol ?1:-1;
        else if(SortIn==='dsc')
            return aCol<bCol ?1:-1
        
    })

    while(table.firstChild)
    {
        table.removeChild(table.firstChild)
    }

    table.appendChild(attr)
    
    rows.forEach(e => {
        table.appendChild(e)
    });
    
    return 
}

